# Hello

---

## How to run app

 - go to project folder in your terminal
 - run ```python3 -m venv venv```
 - run ```source ./venv/bin/activate```
 - run ```pip install -r requirements.txt```
 - run ```export FLASK_APP=app```
 - If you need hot reload run ```export FLASK_ENV=development```
 - copy .env.example to .env and edit with your credentials
 - for seed database run ```flask seed-db```
 - for reset database run ```flask reset-db```
 - for start app run ```flask run```


---

### Endpoint ```/location``` Method ```GET```
### Params 
 - ```?filter=string``` example ```?filter=Samad```
 - ```?location=float numbers seperated by comma``` example ```?location=40.393420, 49.857491```

### Request examples
 - ```http://127.0.0.1:5000/location?filter=Samed&location=40.393420, 49.857491```
 - ```http://127.0.0.1:5000/location?filter=8ci```
 - ```http://127.0.0.1:5000/location?location=40.393420, 49.857491```


# If you have any problem on run app please contact with me

# ```055 939 54 84 Mahammad```
