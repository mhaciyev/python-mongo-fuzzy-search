from .base_seeder import BaseSeeder
from models.location import Location


class LocationSeeder(BaseSeeder):
    def seed(self):
        location = Location()
        location.city = "Baku"
        location.street = "Cahangir Zeynalov küçəsi, AZ 1010"
        location.street_number = "Street number 1"
        location.location = [40.393251, 49.857544]
        location.save()

        location = Location()
        location.city = "Baku"
        location.street = "Təbriz street, AZ 1009"
        location.street_number = "Street number 2"
        location.location = [40.392687, 49.857833]
        location.save()

        location = Location()
        location.city = "Baku"
        location.street = "8-ci mikrorayon Gülarə Qədirbəyova, 1072"
        location.street_number = "Street number 3"
        location.location = [40.4009286, 49.8488356]
        location.save()
