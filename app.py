from flask import Flask
from config.main import configure
from routes.location import location

app = Flask(__name__)
# init configs
configure(app)

app.register_blueprint(location, url_prefix="/location")


@app.route("/")
def hello_world():
    return "<h1>Hello, please go to /location route</h1>"
