from mongoengine import *


class Location(Document):
    city = StringField(max_length=255, required=True)
    street = StringField(required=True)
    street_number = StringField(max_length=255, required=True)
    location = PointField()
