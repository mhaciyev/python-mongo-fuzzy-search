from mongoengine import Q

from models.location import Location
from utils.string import fuzzy_string


def get_locations(**kwargs):
    f = kwargs.get('filter', None)
    l = kwargs.get('location', None)

    _filter = None

    query = Location.objects.all()

    if f is not None:
        f = fuzzy_string(f)
        _filter = Q(street=f) | Q(city=f) | Q(street_number=f)

    if l is not None and len(l.split(',')) == 2:
        try:
            locations = list(map(float, l.split(',')))
            location_fiter = Q(location__near=locations, location__max_distance=1000)
            _filter = location_fiter if _filter is None else _filter & location_fiter
        except Exception as e:
            raise Exception("Location must be string seperated by comma. Location filter is not valid")

    if _filter is not None:
        query = Location.objects(_filter).all()

    return query
