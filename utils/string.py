import re


def fuzzy_string(string):
    characters = ['[aəe]', '[iı]', '[uü]', '[sş]', '[gq]','[çc]']
    string = ".*".join(list(string))
    for char in characters:
        string = re.sub(rf"{char}", rf"{char}", string)

    return re.compile(f".*{string}.*",re.IGNORECASE)
