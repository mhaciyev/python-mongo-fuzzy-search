from dotenv import load_dotenv
from .db import configure_db
from .command import configure_commands


def configure(app):
    load_dotenv()
    configure_db(app)
    configure_commands(app)
