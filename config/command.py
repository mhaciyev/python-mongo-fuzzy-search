from commands.seed import seed
from commands.reset_db import reset_db


def configure_commands(app):
    app.cli.add_command(seed)
    app.cli.add_command(reset_db)
