from flask import jsonify, make_response, request
from services.location_service import get_locations
from resources.location_resource import LocationResource


def locations():
    _filter = dict(request.args).get('filter')
    location = dict(request.args).get('location')
    data = get_locations(filter=_filter, location=location)
    return make_response(jsonify(LocationResource.collect(data)), 200)
