import os

import click
from flask.cli import with_appcontext
from mongoengine import mongodb_support


@click.command(name='reset-db')
@with_appcontext
def reset_db():
    mongo = mongodb_support.get_connection()
    mongo.drop_database('search_service')
